WITH T
AS(
    SELECT  project_management_system.PROJECTS.NAME as PROJECT, project_management_system.PROJECTS.COST as PROJ_MIN_COST, AVG(project_management_system.DEVELOPERS.SALARY) as AVG_SALARY
    FROM project_management_system.DEVELOPERS, project_management_system.PROJECTS
    WHERE project_management_system.DEVELOPERS.PROJECT_ID = project_management_system.PROJECTS.ID
    GROUP BY project_management_system.PROJECTS.NAME, project_management_system.projects.COST)
SELECT PROJECT as PROJ_WITH_MIN_COST, AVG_SALARY as AVERAGE_SALARY  FROM T
WHERE PROJ_MIN_COST  = ANY(SELECT MIN(PROJ_MIN_COST) FROM T)
GROUP BY PROJ_WITH_MIN_COST, AVERAGE_SALARY;