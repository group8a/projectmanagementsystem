ALTER TABLE project_management_system.PROJECTS
  ADD COST REAL;

UPDATE project_management_system.PROJECTS
SET COST = 1900000
WHERE ID = 7;
UPDATE project_management_system.PROJECTS
SET COST = 1850000
WHERE ID = 8;
UPDATE project_management_system.PROJECTS
SET COST = 900000
WHERE ID = 9;
UPDATE project_management_system.PROJECTS
SET COST = 850000
WHERE ID = 10;
UPDATE project_management_system.PROJECTS
SET COST = 950000
WHERE ID = 11;
UPDATE project_management_system.PROJECTS
SET COST = 800000
WHERE ID = 12;