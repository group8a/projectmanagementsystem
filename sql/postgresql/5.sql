/*WITH T
AS(
    SELECT COMPANIES.NAME company, CUSTOMERS.NAME customer, p.COST proj_cost,
           (SELECT MIN(COST) FROM PROJECTS p1 WHERE p1.COMPANY_ID = p.COMPANY_ID) min_proj_cost
    FROM PROJECTS p, COMPANIES, CUSTOMERS
    WHERE p.COMPANY_ID = COMPANIES.ID
          AND p.CUSTOMER_ID = CUSTOMERS.ID)
AND p.FINISH is null
SELECT company, customer FROM T
WHERE min_proj_cost = proj_cost;*/

/* ANY WAY: */

WITH comp_cust_sum(company_name, customer_name, projects_sum)
AS(
    SELECT project_management_system.companies.name, project_management_system.customers.name, SUM(project_management_system.projects.cost)
    FROM project_management_system.companies, project_management_system.customers, project_management_system.projects
    WHERE companies.id=projects.company_id AND
          customers.id=projects.customer_id
    GROUP BY companies.id, customers.id),
    comp_sum_min(company_name, min)
  AS(
      SELECT company_name, MIN(projects_sum) FROM comp_cust_sum
      GROUP BY company_name)
SELECT comp_cust_sum.company_name, comp_cust_sum.customer_name
FROM comp_cust_sum, comp_sum_min
WHERE comp_cust_sum.projects_sum=comp_sum_min.min;