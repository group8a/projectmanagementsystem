/*--------------------------------------------------------------- 
 Task 1
 ---------------------------------------------------------------*/
ALTER TABLE developers
  ADD SALARY REAL;

UPDATE developers
SET SALARY = 5100
WHERE ID = 1;

UPDATE developers
SET SALARY = 6000
WHERE ID = 2;

UPDATE developers
SET SALARY = 3000
WHERE ID = 3;

UPDATE developers
SET SALARY = 3500
WHERE ID = 4;

UPDATE developers
SET SALARY = 3700
WHERE ID = 5;

UPDATE developers
SET SALARY = 2900
WHERE ID = 6;

UPDATE developers
SET SALARY = 7000
WHERE ID = 7;

UPDATE developers
SET SALARY = 5000
WHERE ID = 8;

UPDATE developers
SET SALARY = 4000
WHERE ID = 9;

UPDATE developers
SET SALARY = 5800
WHERE ID = 10;

UPDATE developers
SET SALARY = 6700
WHERE ID = 11;

UPDATE developers
SET SALARY = 6200
WHERE ID = 12;

UPDATE developers
SET SALARY = 2800
WHERE ID = 13;

UPDATE developers
SET SALARY = 3500
WHERE ID = 14;

UPDATE developers
SET SALARY = 1700
WHERE ID = 15;

UPDATE developers
SET SALARY = 2500
WHERE ID = 16;

UPDATE developers
SET SALARY = 1900
WHERE ID = 17;

UPDATE developers
SET SALARY = 1700
WHERE ID = 18;

UPDATE developers
SET SALARY = 3300
WHERE ID = 19;

UPDATE developers
SET SALARY = 2700
WHERE ID = 20;
/*--------------------------------------------------------------- 
 Task 2
 ---------------------------------------------------------------*/
WITH T
AS (
    SELECT
      project_management_system.PROJECTS.NAME          AS PROJECT,
      SUM(project_management_system.DEVELOPERS.SALARY) AS COST_BY_SALARY
    FROM project_management_system.DEVELOPERS
      INNER JOIN project_management_system.PROJECTS ON DEVELOPERS.PROJECT_ID = PROJECTS.ID
    GROUP BY PROJECTS.NAME)
SELECT
  PROJECT,
  COST_BY_SALARY
FROM T
WHERE COST_BY_SALARY = ANY (SELECT MAX(COST_BY_SALARY)
                            FROM T);
/*--------------------------------------------------------------- 
 Task 3
 ---------------------------------------------------------------*/
SELECT
  project_management_system.SKILLS.NAME            AS SKILL,
  SUM(project_management_system.DEVELOPERS.SALARY) AS SUM_SALARY
FROM project_management_system.DEVELOPERS, project_management_system.DEVELOPERS_SKILLS, project_management_system.SKILLS
WHERE
  project_management_system.DEVELOPERS.ID = project_management_system.DEVELOPERS_SKILLS.DEVELOPER_ID
  AND project_management_system.DEVELOPERS_SKILLS.SKILL_ID = SKILLS.ID
  AND project_management_system.SKILLS.NAME LIKE 'Java'
GROUP BY project_management_system.SKILLS.NAME;
/*--------------------------------------------------------------- 
 Task 4
 ---------------------------------------------------------------*/
ALTER TABLE project_management_system.PROJECTS
  ADD COST REAL;

UPDATE project_management_system.PROJECTS
SET COST = 1900000
WHERE ID = 7;
UPDATE project_management_system.PROJECTS
SET COST = 1850000
WHERE ID = 8;
UPDATE project_management_system.PROJECTS
SET COST = 900000
WHERE ID = 9;
UPDATE project_management_system.PROJECTS
SET COST = 850000
WHERE ID = 10;
UPDATE project_management_system.PROJECTS
SET COST = 950000
WHERE ID = 11;
UPDATE project_management_system.PROJECTS
SET COST = 800000
WHERE ID = 12;
/*--------------------------------------------------------------- 
 Task 5
 ---------------------------------------------------------------*/
WITH comp_cust_sum(company_name, customer_name, projects_sum)
AS(
    SELECT project_management_system.companies.name, project_management_system.customers.name, SUM(project_management_system.projects.cost)
    FROM project_management_system.companies, project_management_system.customers, project_management_system.projects
    WHERE companies.id=projects.company_id AND
          customers.id=projects.customer_id
    GROUP BY companies.id, customers.id),
    comp_sum_min(company_name, min)
  AS(
      SELECT company_name, MIN(projects_sum) FROM comp_cust_sum
      GROUP BY company_name)
SELECT comp_cust_sum.company_name, comp_cust_sum.customer_name
FROM comp_cust_sum, comp_sum_min
WHERE comp_cust_sum.projects_sum=comp_sum_min.min;
/*--------------------------------------------------------------- 
 Task 6
 ---------------------------------------------------------------*/
WITH T
AS(
    SELECT  project_management_system.PROJECTS.NAME as PROJECT, project_management_system.PROJECTS.COST as PROJ_MIN_COST, AVG(project_management_system.DEVELOPERS.SALARY) as AVG_SALARY
    FROM project_management_system.DEVELOPERS, project_management_system.PROJECTS
    WHERE project_management_system.DEVELOPERS.PROJECT_ID = project_management_system.PROJECTS.ID
    GROUP BY project_management_system.PROJECTS.NAME, project_management_system.projects.COST)
SELECT PROJECT as PROJ_WITH_MIN_COST, AVG_SALARY as AVERAGE_SALARY  FROM T
WHERE PROJ_MIN_COST  = ANY(SELECT MIN(PROJ_MIN_COST) FROM T)
GROUP BY PROJ_WITH_MIN_COST, AVERAGE_SALARY;