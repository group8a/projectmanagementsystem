SELECT
  project_management_system.SKILLS.NAME            AS SKILL,
  SUM(project_management_system.DEVELOPERS.SALARY) AS SUM_SALARY
FROM project_management_system.DEVELOPERS, project_management_system.DEVELOPERS_SKILLS, project_management_system.SKILLS
WHERE
  project_management_system.DEVELOPERS.ID = project_management_system.DEVELOPERS_SKILLS.DEVELOPER_ID
  AND project_management_system.DEVELOPERS_SKILLS.SKILL_ID = SKILLS.ID
  AND project_management_system.SKILLS.NAME LIKE 'Java'
GROUP BY project_management_system.SKILLS.NAME;