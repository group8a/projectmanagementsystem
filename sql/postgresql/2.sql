WITH T
AS (
    SELECT
      project_management_system.PROJECTS.NAME          AS PROJECT,
      SUM(project_management_system.DEVELOPERS.SALARY) AS COST_BY_SALARY
    FROM project_management_system.DEVELOPERS
      INNER JOIN project_management_system.PROJECTS ON DEVELOPERS.PROJECT_ID = PROJECTS.ID
    GROUP BY PROJECTS.NAME)
SELECT
  PROJECT,
  COST_BY_SALARY
FROM T
WHERE COST_BY_SALARY = ANY (SELECT MAX(COST_BY_SALARY)
                            FROM T);