package ua.goit.dao.jdbc;

import ua.goit.dao.DeveloperDAO;
import ua.goit.entity.Developer;

import javax.sql.DataSource;
import java.awt.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;


public class DeveloperDaoJdbc implements DeveloperDAO {

    private static final String GET_ALL = "SELECT id, name, age, address, join_date, salary, project_id, company_id FROM project_management_system.developers";
    private static final String GET_BY_ID = "SELECT id, name, age, address, join_date, salary, project_id, company_id FROM project_management_system.developers WHERE id = ?";
    private static final String GET_BY_SALARY = "SELECT id, name, age, address, join_date, salary, project_id, company_id FROM project_management_system.developers WHERE salary > ?";
    private static final String INSERT_NEW = "INSERT INTO project_management_system.DEVELOPERS(id, name, age, address, salary, project_id, company_id ) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String DELETE_ROW = "DELETE FROM project_management_system.developers WHERE id = ?";
    private static final String DELETE_PC_ROW = "DELETE FROM project_management_system.developers_skills WHERE developer_id = ?";
    private DataSource dataSource;
    private static final String UPDATE_ROW = "update project_management_system.developers set salary = ? where id = ?";

    public DeveloperDaoJdbc(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    Connection getConnection()
            throws SQLException {
        return dataSource.getConnection();
    }

    public Collection<Developer> getAll() {
        try {
            try (Connection connection = getConnection()) {
                try (Statement ps = connection.createStatement()) {
                    Collection<Developer> out = new ArrayList<>();
                    try (ResultSet resultSet = ps.executeQuery(GET_ALL)) {
                        while (resultSet.next()) {
                            Developer developer = new Developer();
                            developer.setId(resultSet.getLong(1));
                            developer.setName(resultSet.getString(2));
                            developer.setAge(resultSet.getInt(3));
                            developer.setAddress(resultSet.getString(4));
                            developer.setJoin_data(resultSet.getString(5));
                            developer.setSalary(resultSet.getFloat(6));
                            developer.setProject_id(resultSet.getInt(7));
                            developer.setCompany_id(resultSet.getInt(8));
                            out.add(developer);
                        }
                    }
                    return out;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public Developer getDeveloperById(long id) {
        try {
            try (Connection connection = getConnection()) {
                try (PreparedStatement ps = connection.prepareStatement(GET_BY_ID)) {
                    ps.setLong(1, id);
                    try (ResultSet resultSet = ps.executeQuery()) {
                        if (!resultSet.next()) {
                            return null;
                        }
                        Developer developer = new Developer();
                        developer.setId(resultSet.getLong(1));
                        developer.setName(resultSet.getString(2));
                        developer.setAge(resultSet.getInt(3));
                        developer.setAddress(resultSet.getString(4));
                        developer.setJoin_data(resultSet.getString(5));
                        developer.setSalary(resultSet.getFloat(6));
                        developer.setProject_id(resultSet.getInt(7));
                        developer.setCompany_id(resultSet.getInt(8));
                        return developer;
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public Collection<Developer> getDeveloperBySalary(float salary) {
        try {
            try (Connection connection = getConnection()) {
                try (PreparedStatement ps = connection.prepareStatement(GET_BY_SALARY)) {
                    ps.setFloat(1, salary);
                    Collection<Developer> out = new ArrayList<>();
                    try (ResultSet resultSet = ps.executeQuery()) {
                        while (resultSet.next()) {
                            Developer developer = new Developer();
                            developer.setId(resultSet.getLong(1));
                            developer.setName(resultSet.getString(2));
                            developer.setAge(resultSet.getInt(3));
                            developer.setAddress(resultSet.getString(4));
                            developer.setJoin_data(resultSet.getString(5));
                            developer.setSalary(resultSet.getFloat(6));
                            developer.setProject_id(resultSet.getInt(7));
                            developer.setCompany_id(resultSet.getInt(8));
                            out.add(developer);
                        }
                        return out;
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }


    public Developer add(long id, String name, int age, String address, float salary, int project_id, int company_id) {
        try {
            try (Connection connection = getConnection()) {
                try (PreparedStatement ps = connection.prepareStatement(INSERT_NEW)) {
                    ps.setLong(1, id);
                    ps.setString(2, name);
                    ps.setInt(3, age);
                    ps.setString(4, address);

                    ps.setFloat(5, salary);
                    ps.setInt(6, project_id);
                    ps.setInt(7, company_id);

                    if (!ps.execute()) {
                        return null;
                    }
                    try (ResultSet resultSet = ps.getResultSet()) {
                        if (!resultSet.next()) {
                            return null;
                        }

                    Developer developer = new Developer();
                    developer.setId(id);
                    developer.setName(name);
                    developer.setAge(age);
                    developer.setAddress(address);
                    developer.setSalary(salary);
                    developer.setProject_id(project_id);
                    developer.setCompany_id(company_id);
                    return developer;
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public boolean delete(long id) {
        try {
            try (Connection connection = getConnection()) {
                try {
                    connection.setAutoCommit(false);
                    try (PreparedStatement ps = connection.prepareStatement(DELETE_PC_ROW)) {
                        ps.setLong(1, id);
                        ps.executeUpdate();
                    }
                    boolean removed;
                    try (PreparedStatement ps = connection.prepareStatement(DELETE_ROW)) {
                        ps.setLong(1, id);
                        removed = ps.executeUpdate() > 0;
                    }
                    connection.commit();
                    return removed;
                } catch (Exception e) {
                    connection.rollback();
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }


    public boolean update(Developer developer) {
        try {
            try (Connection connection = getConnection()) {
                try (PreparedStatement ps = connection.prepareStatement(UPDATE_ROW)) {
                    ps.setFloat(1, developer.getSalary());
                    ps.setLong(2, developer.getId());

                    return ps.executeUpdate() > 0;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
