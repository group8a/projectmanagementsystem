package ua.goit;


import org.apache.commons.dbcp.BasicDataSource;
import ua.goit.dao.jdbc.DeveloperDaoJdbc;
import ua.goit.entity.Developer;

public class Main {
    private static final String URL = "jdbc:postgresql://localhost/postgres";
    public static void main(String[] args) {
        String username = System.getProperty("username");
        String password = System.getProperty("password");
        //Navigation Bar -> Edit Configurations... -> VM Options: -Dusername= -Dpassword=

        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(URL);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setMaxActive(10);
        dataSource.setMaxIdle(2);

        DeveloperDaoJdbc developerDaoJdbc = new DeveloperDaoJdbc(dataSource);
        System.out.println("\n------------------- Get ALL developer ---------------------");
        developerDaoJdbc.getAll().forEach(System.out::println);
        System.out.println("\n------------------- Get by ID developer 1, 17 ---------------------");
        System.out.println(developerDaoJdbc.getDeveloperById(1));
        System.out.println(developerDaoJdbc.getDeveloperById(17));

        System.out.println("\n------------------- Get by SALARY developer > 6000 ---------------------");
        developerDaoJdbc.getDeveloperBySalary(6000).forEach(System.out::println);

        System.out.println("\n------------------- DELETE developer ---------------------");
        System.out.println(developerDaoJdbc.delete(21));

        System.out.println("\n------------------- INSERT developer ---------------------");
        System.out.println(developerDaoJdbc.add(21,"Jobs", 55, "Polo Alto", 999,10,333));
        System.out.println("\n------------------- TEST ---------------------");
        System.out.println(developerDaoJdbc.getDeveloperById(21));
//        System.out.println("\n------------------- DELETE developer ---------------------");
//        System.out.println(developerDaoJdbc.delete(21));
        System.out.println("\n------------------- UPDATE developer ---------------------");
        Developer developer = new Developer(21,"Jobs", 55, "Polo Alto", 1,10,333);
        System.out.println(developerDaoJdbc.update(developer));
        System.out.println("\n------------------- TEST ---------------------");
        System.out.println(developerDaoJdbc.getDeveloperById(21));

    }
}
