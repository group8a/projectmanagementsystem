package ua.goit.entity;

import javax.persistence.*;

/**
 * Created by Artur on 17.12.2016.
 */
@Entity
@Table(name = "skills", schema = "project_management_system")
public class Skills {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
