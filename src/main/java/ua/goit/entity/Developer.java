package ua.goit.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Artur on 17.12.2016.
 */
@Entity
@Table(name = "developer", schema = "project_management_system")
public class Developer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private int age;
    private String address;
    private String join_data;
    private float salary;

    public Developer(long id, String name, int age, String address, float salary, int project_id, int company_id) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
        this.salary = salary;
        this.project_id = project_id;
        this.company_id = company_id;
    }
    public Developer() {
    }
    @OneToMany
    private int project_id;
    private int company_id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJoin_data() {
        return join_data;
    }

    public void setJoin_data(String join_data) {
        this.join_data = join_data;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                ", join_data='" + join_data + '\'' +
                ", salary=" + salary +
                ", project_id=" + project_id +
                ", company_id=" + company_id +
                '}';
    }
}
